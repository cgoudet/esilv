# Ressources

This document list multiple ressources related to various apect of the course.

## Tutos

* install python and jupyter
  - on windows : [https://www.youtube.com/watch?v=otmWEEFysms](https://www.youtube.com/watch?v=otmWEEFysms)
  - on linux : [https://www.youtube.com/watch?v=Yg9AkozItTU](https://www.youtube.com/watch?v=Yg9AkozItTU)
  - on Mac : [https://www.youtube.com/watch?v=XUaJ8OctxdM](https://www.youtube.com/watch?v=XUaJ8OctxdM)

* tuto Git : [https://www.hostinger.fr/tutoriels/tuto-git/](https://www.hostinger.fr/tutoriels/tuto-git/)
* tuto numpy : [https://www.youtube.com/watch?v=ZB7BZMhfPgk](https://www.youtube.com/watch?v=ZB7BZMhfPgk)
* tuto broadcasting numpy : [https://numpy.org/devdocs/user/theory.broadcasting.html](https://numpy.org/devdocs/user/theory.broadcasting.html)
* tuto pandas : [https://www.youtube.com/watch?v=5rNu16O3YNE](https://www.youtube.com/watch?v=5rNu16O3YNE)

* tuto sklearn : [https://www.youtube.com/watch?v=0Lt9w-BxKFQ&t=1098s](https://www.youtube.com/watch?v=0Lt9w-BxKFQ) 

## Data science concepts

* dimension reduction : [https://www.youtube.com/watch?v=nq6iPZVUxZU](https://www.youtube.com/watch?v=nq6iPZVUxZU)

## Best practices
* good programming practices : [https://www.youtube.com/watch?v=ZsHMHukIlJY](https://www.youtube.com/watch?v=ZsHMHukIlJY)
* cookiecutter for data science project organisation : [https://drivendata.github.io/cookiecutter-data-science/](https://drivendata.github.io/cookiecutter-data-science/)
