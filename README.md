# Applied data analysis in python

This repository proposes a course on data analysis with python.
It relies on multiple concepts of the python language and specific libraries.
All along the course, link to tutorials are provided to allow readers to go deeper in concepts which are not the main concern of the course.
However, most of the content is not python specific.
The readers can reuse the concepts in any language of their choice.

The course is organised in a dozen chapters.
Each chapter contains a description of concepts and their application on real data.
Solved problems are also proposed for readers to train on.

The course content is organised into jupyter notebooks.
They can be visualised directly on GitLab under the directory ```notebooks/report```.
The notebooks are then ordered in the intended course order.

The course content can be integrally read on the github pages in a non interactive way.

In order to run the notebooks locally, follow the installation section.

Suggestions and comments are welcome!

## Installation
Several software engineering notions are required to run those notebooks.
Readers not aware of them are strongly encouraged to learn those first.
More advanced readers can just skip first steps.

### Main software installation

You need 3 components to starts working with this project on your own machine.
- python 3.10.9
- [GIT](https://www.youtube.com/watch?v=DVRQoVRzMIY) : a must have tool for developpers to keep track of their code changes.
- a code editor (for example visual studio code)

#### Mac & Linux
On Mac and Linux, git is already installed by default.

Python is already installed on all Mac and Linux distributions but may not be in a version that support all the libraries of the project.
Run the following lines in a terminal to install the correct version.

```
brew install python3.10.9 # Mac

apt-get install python3.10.9 # Linux
```

Finally you need to install a software dedicated to write code.
For example, Visual Studio code can be downloaded on the following link : [https://code.visualstudio.com/](https://code.visualstudio.com/)


#### Windows

##### Python
Python is not directly usable on Windows by default.
In order to install it, got to this [page](https://www.python.org/downloads/windows/) and download the executable for the version 3.10.9.
Double click on the downloaded file and follow the installation steps.
When you reach the following screen, make sure to check the box to "Add Python to PATH".

![critical installation screen](https://docs.blender.org/manual/fr/2.81/_images/about_contribute_install_windows_installer.png)

Open a terminal by searching for the application `CMD`.
When you type `python` and hit enter you should end up with a screen similar to the following image.

![Python is opend in windows terminal](https://www.tutorialsteacher.com/Content/images/python/python-shell.png)

##### Git
Go to the git [website](https://gitforwindows.org/) and download a windows version of GIT.
Run the installation procedure by keeping all the default parameters.

##### VS Code
Get and install visual studio code on the following website : [https://code.visualstudio.com/](https://code.visualstudio.com/).

### Installing the project

In a terminal (or the "GIT bash" tool on windows), use the command `cd <dirname>` to move to the directory where you want the code to be copied.
Then run :
```bash
git clone https://gitlab.com/cgoudet/esilv.git
```
This will create a new directory `data-analysis` which contain all the course content.

Then you need to install the python libraries which are used by the course.
To avoid libraries conflicts with other projects, they are installed in a specific environment.
```bash
# install virtualenv and wheel system-wide
pip install virtualenv wheel

# create virtual environment
virtualenv venv -p python3.10.9

# activate the libraries; must be done everytime
# on linux
source venv/bin/activate
# on windows
venv\Scripts\activate

# install libraries and various code quality tools
pip install -r requirements.txt
pre-commit install
jupyter nbextension install https://github.com/drillan/jupyter-black/archive/master.zip --user
jupyter nbextension enable jupyter-black-master/jupyter-black
```

The course heavily make use of the jupyter notebooks.
Those notebooks propose an interface to run easily run python code in an intuitive way.
Notebooks are heavily used in data science, in particular to explore data.
This video proposes an introduction to [jupyter notebooks](https://www.dataquest.io/blog/jupyter-notebook-tutorial/).

In the `data-analysis` directory, with the virtual environment activated, run
```
jupyter notebook
```

This will open a tab in you web browser and allow you to open notebooks in the `notebooks/report` directory and run the code.
