import re

import spacy

pat_punct = re.compile(r"[^\w\s]")
pat_space = re.compile(r" {1,}")
sp_en = spacy.load("en")


def preproc_text(txt):
    txt = pat_punct.sub(" ", txt)
    txt = txt.lower()
    txt = pat_space.sub(" ", txt)
    txt = txt.strip()
    sentence = sp_en(txt)
    sentence = [
        word.lemma_ for word in sentence if word not in sp_en.Defaults.stop_words
    ]
    txt = " ".join(sentence)
    return txt
