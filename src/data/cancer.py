from sklearn.datasets import load_breast_cancer

from src import INTDIR


def read_dataset():
    df, target = load_breast_cancer(as_frame=True, return_X_y=True)
    df["target"] = target
    return df


def transform(df):
    return df.rename(columns=lambda c: c.replace(" ", "_"))


def pipeline():
    df = read_dataset().pipe(transform)
    outfn = INTDIR / "breast_cancer.parquet"
    df.to_parquet(outfn)


if __name__ == "__main__":
    pipeline()
