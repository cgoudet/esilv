import base64
import json

import pandas as pd
import requests

from src import INTDIR, RAWDIR


def get_dataset():
    r = requests.get(
        "https://gitlab.com/api/v4/projects/24536021/repository/files/healthcare_startup%2Fdata_extract.csv?ref=master"
    )
    assert r.status_code == 200
    content = json.loads(r.content.decode("utf-8"))

    with open(RAWDIR / "patients.csv", "wb") as f:
        f.write(base64.b64decode(content["content"]))


def read_dataset():
    fn = RAWDIR / "patients.csv"
    df = pd.read_csv(fn)
    return df


def pipeline():
    get_dataset()
    df = read_dataset()

    outfn = INTDIR / "patients.parquet"
    df.to_parquet(outfn)


if __name__ == "__main__":
    pipeline()
