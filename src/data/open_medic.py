import pandas as pd

from src import INTDIR, RAWDIR, ROOTDIR


def read_dataset():

    fn = RAWDIR / "OPEN_MEDIC_2019.zip"
    df = pd.read_csv(
        fn,
        sep=";",
        encoding="latin1",
        decimal=",",
        thousands=".",
        dtype={"TOP_GEN": str},
    )
    return df.rename(columns=str.lower)


def add_prescriptor(df):
    match_prescriptor = pd.read_csv(
        ROOTDIR / "references" / "open_medic_match_prescriptor.csv"
    )
    dict_prescriptor = {k: v for k, v in match_prescriptor.values}
    return df.assign(l_psp_spe=lambda df: df["psp_spe"].map(dict_prescriptor)).drop(
        columns=["psp_spe"]
    )


def add_region(df):
    match_region = pd.read_csv(
        ROOTDIR / "references" / "open_medic_match_region.csv", index_col=0
    )["value"].to_dict()
    return df.assign(l_ben_reg=lambda df: df["ben_reg"].map(match_region)).drop(
        columns=["ben_reg"]
    )


def add_features(df):
    df = (
        df.fillna({"l_cip13": "Unknown"})
        .pipe(add_prescriptor)
        .pipe(add_region)
        .assign(
            name=lambda frame: frame["l_cip13"].apply(lambda x: x.split(" ")[0]),
            taux_rembourse=lambda frame: frame["rem"] / df["bse"],
        )
    )
    return df


def pipeline():
    df = read_dataset().pipe(add_features)
    outfn = INTDIR / "OPEN_MEDIC_2019.parquet"
    df.to_parquet(outfn)


if __name__ == "__main__":
    pipeline()
